﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Novacode;
using RazorEngine;
using RazorEngine.Templating;
using System.Text.RegularExpressions;

namespace LetterOfEngagementPOC
{
    class Program
    {
        static void Main(string[] args)
        {
            UpdateDocument();
        }

        /// <summary>
        /// 1. Loads a template
        /// 2. Loops through paragraphs
        /// 3. Looks for [[ foo ]] where foo is a Razor Expression
        /// 4. Complies and replaces the Razor Expression with new text
        /// 5. Leaves formatting intact except for the removal of a yellow highlight
        /// 
        /// TODO
        /// 
        /// - Allow replacements to use RazorEngine
        /// - See if formatting can be changed
        /// - Make sure all files are cleaned
        /// - Handle template caching (hash the template text?)
        /// - Gracefully handle errors in the Word template
        /// - Ensure changes to text in shapes are replaced
        /// - Ensure changes to text in tables are replaced

        /// </summary>
        static void UpdateDocument()
        {
            using (var document = DocX.Load("Templates/Template Revised 12.docx"))
            {

                foreach (Paragraph p in document.Paragraphs)
                {
                    var formatting = new Formatting()
                    {
                        Highlight = Highlight.none
                    };

                    p.ReplaceText( @"\[\[(.*?)\]\]", ReplaceRazor, newFormatting: formatting);
                }

                document.SaveAs("Templates/Updated.docx");

                Console.WriteLine("Done.");
                Console.ReadLine();
            }
        }

        static string ReplaceRazor(string template)
        {
            var model = new
            {
                ContactName = "Nathan Murray",
                ContactAddress1 = "9606 Livenshire Drive",
                Properties = new List<string>() { "2100 Ross Ave", "1600 Pennsylvania Ave", "123 Main Street"}
            };
            var templateKey = new Random().NextDouble().ToString();
            var result = Engine.Razor.RunCompile(template, templateKey, null, model);
            return result;
        }
    }

}
